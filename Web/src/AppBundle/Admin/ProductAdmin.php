<?php
/**
 * Created by PhpStorm.
 * User: chepe
 * Date: 5/19/15
 * Time: 12:33 PM
 */

namespace AppBundle\Admin;

use Demi\UserBundle\Entity\User;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class ProductAdmin extends Admin {

// Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper
            ->add('name')
            ->add('description')
            ->add('price')
            ->add('quantity')
            ->add('image', 'file', array('required' => false, 'data_class' => null, 'mapped' => true))
            ->add('enabled')
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('description')
            ->add('price')
            ->add('quantity')
            ->add('enabled')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->add('description')
            ->add('price')
            ->add('quantity')
            ->add('image', null, array(
                'template' => 'AppBundle:Admin:image.html.twig'
            ))
            ->add('enabled')
        ;
    }

    public function prePersist($object)
    {
        // We get the uploadable manager!
        $uploadableManager = $this->getConfigurationPool()->getContainer()->get('stof_doctrine_extensions.uploadable.manager');
        $uploadableManager->markEntityToUpload($object, $object->getImage());
    }

    public function preUpdate($object)
    {
        $DM = $this->getConfigurationPool()->getContainer()->get('Doctrine')->getManager();
        $uow = $DM->getUnitOfWork();
        $OriginalEntityData = $uow->getOriginalEntityData( $object );

        if( $object->getImage() != null ) {
            // We get the uploadable manager!
            $uploadableManager = $this->getConfigurationPool()->getContainer()->get('stof_doctrine_extensions.uploadable.manager');
            $uploadableManager->markEntityToUpload($object, $object->getImage());
        } else {
            $object->setImage( $OriginalEntityData->getImage() );
        }
    }

}