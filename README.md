To run this project, go to the Web folder:

$ cd Web

Then get Composer (https://getcomposer.org/) via the script already in place:

$ bash getcomposer.sh

Install all the dependencies:

$ php composer.phar install

Configure your database in the app/config/parameters.yml if needed.

Add permissions to the cache and logs folder:

$ chmod -R 777 app/cache/ app/logs/